// CellDetector.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "adressbook.h"
#include <chrono>
#include <iostream>
#include <vector>
#include <thread>

using namespace foundry;

int main(int argc, char *argv[])
{
    try {
        srand(time(0));
        constexpr size_t N(100000);
        auto start = std::chrono::steady_clock::now();
        auto end = std::chrono::steady_clock::now();
        std::chrono::duration<double, std::milli> elapsed_seconds = end - start;

        const std::vector<std::string> letters = []() {
            std::vector<std::string> l_res;
            l_res.reserve(26);
            for (char ch('a'); ch <= 'z'; ++ch) {
                l_res.push_back(std::string(1, ch));
            }
            return l_res;
        }();

        std::vector<Contact> l_vec_contact;
        for (size_t i(0); i < N; ++i) {
            const std::string l_firstName = letters[rand() % 26] + letters[rand() % 26]
                                            + letters[rand() % 26] + letters[rand() % 26] + letters[rand() % 26];
            const std::string l_lastName = letters[rand() % 26] + letters[rand() % 26]
                                           + letters[rand() % 26] + letters[rand() % 26] + letters[rand() % 26];
            l_vec_contact.push_back(Contact(l_firstName, l_lastName));
        }

        AdressBook l_book;

        /// Insert Benchmark ---------------------------------------------------------------------------------
        start = std::chrono::steady_clock::now();
        for (const auto &l_it : l_vec_contact) {
            l_book.insert(l_it);
        }
        end = std::chrono::steady_clock::now();
        elapsed_seconds = end - start;
        std::cout << "Insert Benchmark time: " << elapsed_seconds.count()/N << "ms\n";

        /// Sort Benchmark ---------------------------------------------------------------------------------
        start = std::chrono::steady_clock::now();
        for(int i(0); i < 100; ++i){
            l_book.sort(ContactComparator::SortOrder::SortByLastName);
            l_book.sort(ContactComparator::SortOrder::SortByFirstName);
        }
        end = std::chrono::steady_clock::now();
        elapsed_seconds = end - start;
        std::cout << "Sort Benchmark time: " << elapsed_seconds.count()/(2.*100) << "ms\n";


        /// Search Benchmark ---------------------------------------------------------------------------------
        start = std::chrono::steady_clock::now();
        std::vector<AdressBook::iterator> l_foundContact;
        for(int i(0); i < 100; ++i){
            l_foundContact = l_book.findName(l_vec_contact[rand() % N].firstName().substr(0,2));
        }
        end = std::chrono::steady_clock::now();
        elapsed_seconds = end - start;
        std::cout << "Search Benchmark time: " << elapsed_seconds.count()/100 << "ms\n";

        // Display found contact
        for (const auto &l_it : l_foundContact) {
            std::cout << l_it->firstName() << " " << l_it->lastName() << "\n";
        }

        /// Delete Benchmark ---------------------------------------------------------------------------------
        std::cout << l_book.size() << std::endl;
        start = std::chrono::steady_clock::now();
        for(const auto &l_it : l_foundContact){
            l_book.remove(l_it);
        }
        // Iterator are invalidated !!
        l_foundContact.clear();
        end = std::chrono::steady_clock::now();
        elapsed_seconds = end - start;
        std::cout << "Delete Benchmark time: " << elapsed_seconds.count()/(l_foundContact.size()) << "ms\n";
        std::cout << l_book.size() << std::endl;


        /// MultiThread Test ---------------------------------------------------------------------------------
        std::cout << l_book.size() << std::endl;
        auto l_addingThread = std::thread([&l_book](){
            std::cout << "Hello from adding thread ms\n";
            for(int i(0); i < 1000; ++i){
                l_book.insert(Contact("hello", "world"));
            }
        });
        auto l_removingThread = std::thread([&l_book](){
            std::cout << "Hello from removing thread ms\n";
            for(int i(0); i < 500; ++i){
                l_book.remove(l_book.begin());
            }
        });

        l_addingThread.join();
        l_removingThread.join();
        std::cout << l_book.size() << std::endl;

        std::cout << "End of Program" <<std::endl;

    } catch (const std::exception &exception) {
        std::cerr << exception.what() << std::endl;
    }

    return 0;
}
