#include "adressbook.h"
#include <iostream>
#include <cctype>
#include <algorithm>

namespace foundry {

AdressBook::iterator AdressBook::begin()
{
    return m_book.begin();
}

AdressBook::iterator AdressBook::end()
{
    return m_book.end();
}

AdressBook::const_iterator AdressBook::begin() const
{
    return m_book.begin();
}

AdressBook::const_iterator AdressBook::end() const
{
    return m_book.end();
}


size_t AdressBook::size() const{
    return m_book.size();
}

// Nlog(N). // N = size of list
void AdressBook::sort(const ContactComparator::SortOrder a_sortMethod)
{
    if (a_sortMethod == m_sort) {
        return;
    }
    m_sort = a_sortMethod;
    m_book = container(begin(), end(), ContactComparator(a_sortMethod));
}

std::vector<AdressBook::iterator> AdressBook::findName(const std::string &a_searchWord)
{
    std::vector<iterator> l_res;

    auto doFindName = [&a_searchWord](const Contact &a_contact){
        std::string l_tempFirstName(a_contact.firstName()), l_tempLastName(a_contact.lastName());
        std::transform(l_tempFirstName.begin(), l_tempFirstName.end(), l_tempFirstName.begin(), ::tolower);
        std::transform(l_tempLastName.begin(), l_tempLastName.end(), l_tempLastName.begin(), ::tolower);

        if (l_tempFirstName.rfind(a_searchWord, 0) != std::string::npos
            || l_tempLastName.rfind(a_searchWord, 0) != std::string::npos) {
            return true;
        } else {
            return false;
        }
    };

    for(auto l_it(begin()); l_it != end(); ++l_it){
        if(doFindName(*l_it)){
            l_res.push_back(l_it);
        }
    }

    return l_res;
}

void AdressBook::insert(const Contact &a_contact)
{
    std::unique_lock<std::shared_mutex> l_slm(m_mutex);
    m_book.insert({a_contact});
}

bool AdressBook::remove(const iterator &a_it_contact)
{
    std::unique_lock<std::shared_mutex> l_slm(m_mutex);
    m_book.erase(a_it_contact);
    return true;
}

} // namespace foundry
