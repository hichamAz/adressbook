#ifndef FOUNDRY_ADRESS_BOOK_CONFIG_H
#define FOUNDRY_ADRESS_BOOK_CONFIG_H

#include <list>
#include <set>
#include <shared_mutex>
#include "contact.h"

namespace foundry {

class FOUNDRY_FOUNDRYCORE_EXPORT AdressBook
{
public:
    using pointer = std::shared_ptr<AdressBook>;
    using value_type = Contact;
    using container = std::multiset<value_type, ContactComparator>;
    using iterator = container::iterator;
    using const_iterator = container::const_iterator;
    // Worst case complexity         std::multiset vs std::vector
    //          Insert                  O(log(N))         O(N)
    // Search  FirstName + LastName     O(log(N))         O(N)
    //          Remove                  O(log(N))         O(N)
    //          Sort               already sorted         O(Nlog(N))

    // std::vector need sorting after each insert
    // another solution is to use std::lower_bound (for middle insertion) ...

    // After benchmarking AdressBok with big N (size)
    // std::multiset could be a little bit less performant than std::vector
    // but std::multiset is already sorted event after insterting element
    // removing and adding Contact doesn't create big memory reallocation

    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;
    size_t size() const;

    void insert(const Contact& a_contact);
    void sort(const ContactComparator::SortOrder a_sortMethod);
    bool remove(const iterator &a_it_contact);
    std::vector<AdressBook::iterator> findName(const std::string& a_searchWord);

private:
    container m_book;
    std::shared_mutex m_mutex;
    ContactComparator::SortOrder m_sort{ContactComparator::SortOrder::SortByFirstName};
};


} // namespace foundry

#endif //FOUNDRY_ADRESS_BOOK_CONFIG_H
