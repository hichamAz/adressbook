#include "contact.h"


namespace foundry {

Contact::Contact(const std::string &a_firstName,
                 const std::string &a_lastName,
                 const size_t a_phoneNumber) :
      m_firstName(a_firstName),
      m_lastName(a_lastName),
      m_phoneNumber(a_phoneNumber)
{}

std::string Contact::firstName() const
{
    return m_firstName;
}

void Contact::setFirstName(const std::string &a_firstName)
{
    m_firstName = a_firstName;
}

std::string Contact::lastName() const
{
    return m_lastName;
}

void Contact::setLastName(const std::string &a_lastName)
{
    m_lastName = a_lastName;
}

size_t Contact::phoneNumber() const
{
    return m_phoneNumber;
}

void Contact::setPhoneNumber(const size_t a_phoneNumber)
{
    m_phoneNumber = a_phoneNumber;
}

std::shared_ptr<image> Contact::profilePicture() const
{
    return m_profilePicture;
}

void Contact::setProfilePicture(const std::shared_ptr<image> profilePicture)
{
    m_profilePicture = profilePicture;
}


}
