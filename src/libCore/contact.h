#ifndef FOUNDRY_CONTACT_CONFIG_H
#define FOUNDRY_CONTACT_CONFIG_H

#include "exportmacro.h"
#include <array>
#include <cstdint>
#include <memory>
#include <numeric>
#include <string>
#include <vector>

namespace foundry {

class image;

class FOUNDRY_FOUNDRYCORE_EXPORT Contact
{
public:
    using pointer = std::shared_ptr<Contact>;

    explicit Contact(const std::string &a_firstName,
                     const std::string &a_lastName,
                     const size_t a_phoneNumber = 0);

    std::string firstName() const;
    void setFirstName(const std::string &a_firstName);

    std::string lastName() const;
    void setLastName(const std::string &a_lastName);

    size_t phoneNumber() const;
    void setPhoneNumber(const size_t a_phoneNumber);

    std::shared_ptr<image> profilePicture() const;
    void setProfilePicture(const std::shared_ptr<image> a_profilePicture);

private:
    std::string m_firstName;
    std::string m_lastName;
    size_t m_phoneNumber;
    std::shared_ptr<image> m_profilePicture;
};

struct ContactComparator
{
    enum class SortOrder : uint8_t{
        SortByFirstName = 0,
        SortByLastName = 1
    };
    explicit ContactComparator(const ContactComparator::SortOrder a_sortOrder = SortOrder::SortByFirstName) :
          m_sortOrder(a_sortOrder)
    {}

    bool operator()(const Contact &lhs, const Contact &rhs) const
    {
        if(m_sortOrder == SortOrder::SortByFirstName){
            return lhs.firstName() < rhs.firstName();
        }
        else if(m_sortOrder == SortOrder::SortByLastName){
            return lhs.lastName() < rhs.lastName();
        }
        else{
            throw std::exception("Sort not handled");
        }
    }
    SortOrder m_sortOrder;
};


} // namespace foundry

#endif //FOUNDRY_CONTACT_CONFIG_H
